$(document).ready(function(){
	$(window).scroll(function() {
		/*	if($(window).scrollTop()>0){
				$('header').addClass('navbar-fixed-top');
			//	$('header').css('margin-top', 0);
			}else{
				$('header').removeClass('navbar-fixed-top');
			//	$('header').css('margin-top', 30-$(window).scrollTop());
			}
		var shouldBeVisible = $(window).scrollTop()>=$('#about').offset().top;
		if (shouldBeVisible) {
			$('header').css("opacity", 1);
			$('header').css("display","block");
		} else if (!shouldBeVisible) {
			var diff =$(window).scrollTop()-$('#about').offset().top;
			diff = diff/100;
			if(diff<=0 && diff>=-1){
				$('header').css("display","block");
				$('header').css("opacity", 1-Math.abs(diff));
			}
			else
				$('header').css("display","none");
		}*/
		$("header>nav.container ul li").removeClass("active");
		if($(window).scrollTop()+30>=$('#contact').offset().top){
			$("header>nav.container ul li:nth-child(5)").addClass("active");
		}
		else if($(window).scrollTop()+30>=$('#plans').offset().top){
			$("header>nav.container ul li:nth-child(4)").addClass("active");
		}
		else if($(window).scrollTop()+30>=$('#advisors').offset().top){
			$("header>nav.container ul li:nth-child(3)").addClass("active");
		}
		else if($(window).scrollTop()+30>=$('#about').offset().top){
			$("header>nav.container ul li:nth-child(2)").addClass("active");
		}
		else if($(window).scrollTop()+30>=$('#home').offset().top){
			$("header>nav.container ul li:nth-child(1)").addClass("active");			
		}
	});
	$("header>nav.container ul li a").click(function(){
		if($(this).attr("tgt")=="#home"){
			/*$('header').removeClass('navbar-fixed-top');*/			
			$('html,body').animate({scrollTop: 0}, 'slow', 'swing');
		}else
			if($('header').hasClass('navbar-fixed-top')){
				$('html,body').animate({scrollTop: $($(this).attr("tgt")).offset().top}, 'slow', 'swing');
			}else{
				$('html,body').animate({scrollTop: $($(this).attr("tgt")).offset().top-$('header').height()}, 'slow', 'swing');
			}
	
	});
	

/* slider animation

	$(".iPhone-slider").carouFredSel({
		items : 1,			//	no of items to be slide at a time
		direction : "up",  	//	direction can be changed.
		scroll : {
			items : 1,		//	
			duration : 500	//	duration for sliding animation
		}
	}); 
	
	setInterval(function(){ // to detect which image is visible in slider.
		var top = $('.iPhone-slider>img:nth-child(1)'); // to get 1st child of the slider.
		console.log(top.attr("id"));
	}, 500); */
	
	/*explore now button click scroll animation */
	$("#btn").click(function(){
		$('html,body').animate({scrollTop: $('#about').offset().top+1}, 1800, 'swing');
	});

	
/* explore now button click scroll animation to make smooth
	$('#btn').click(function(e) {

		// Prevent the default link behavior
		e.preventDefault();

		// Get anchor link and calculate distance from the top
		var dataID = "#about";
		var dataTarget = document.querySelector(dataID);
		var dataSpeed = 1500;
		var dataEasing = 'easeOutCubic';
		var dataURL = true;

		// If the anchor exists
		if (dataTarget) {
			// Scroll to the anchor
			smoothScroll(dataTarget, dataSpeed || 500, dataEasing || 'easeInOutCubic', dataURL || 'false');
		}

	});*/
});

var smoothScroll = function (anchor, duration, easing, url) {

			// Functions to control easing
			var easingPattern = function (type, time) {
				if ( type == 'easeInQuad' ) return time * time; // accelerating from zero velocity
				if ( type == 'easeOutQuad' ) return time * (2 - time); // decelerating to zero velocity
				if ( type == 'easeInOutQuad' ) return time < 0.5 ? 2 * time * time : -1 + (4 - 2 * time) * time; // acceleration until halfway, then deceleration
				if ( type == 'easeInCubic' ) return time * time * time; // accelerating from zero velocity
				if ( type == 'easeOutCubic' ) return (--time) * time * time + 1; // decelerating to zero velocity
				if ( type == 'easeInOutCubic' ) return time < 0.5 ? 4 * time * time * time : (time - 1) * (2 * time - 2) * (2 * time - 2) + 1; // acceleration until halfway, then deceleration
				if ( type == 'easeInQuart' ) return time * time * time * time; // accelerating from zero velocity
				if ( type == 'easeOutQuart' ) return 1 - (--time) * time * time * time; // decelerating to zero velocity
				if ( type == 'easeInOutQuart' ) return time < 0.5 ? 8 * time * time * time * time : 1 - 8 * (--time) * time * time * time; // acceleration until halfway, then deceleration
				if ( type == 'easeInQuint' ) return time * time * time * time * time; // accelerating from zero velocity
				if ( type == 'easeOutQuint' ) return 1 + (--time) * time * time * time * time; // decelerating to zero velocity
				if ( type == 'easeInOutQuint' ) return time < 0.5 ? 16 * time * time * time * time * time : 1 + 16 * (--time) * time * time * time * time; // acceleration until halfway, then deceleration
				return time; // no easing, no acceleration
			};

			// Function to update URL
			var updateURL = function (url, anchor) {
				if ( url === 'true' && history.pushState ) {
					history.pushState(null, null, '#' + anchor.id);
				}
			};

			// Calculate how far and how fast to scroll
			// http://www.quirksmode.org/blog/archives/2008/01/using_the_assig.html
			var startLocation = window.pageYOffset;
			var scrollHeader = document.querySelector( '.scroll-header' );
			var headerHeight = scrollHeader === null ? 0 : scrollHeader.offsetHeight;
			var endLocation = function (anchor) {
				var distance = 0;
				if (anchor.offsetParent) {
					do {
						distance += anchor.offsetTop;
						anchor = anchor.offsetParent;
					} while (anchor);
				}
				return distance - headerHeight;
			};
			var distance = endLocation(anchor) - startLocation;
			var increments = distance / (duration / 16);
			var timeLapsed = 0;
			var percentage, position, stopAnimation;

			// Scroll the page by an increment, and check if it's time to stop
			var animateScroll = function () {
				timeLapsed += 16;
				percentage = ( timeLapsed / duration );
				percentage = ( percentage > 1 ) ? 1 : percentage;
				position = startLocation + ( distance * easingPattern(easing, percentage) );
				window.scrollTo(0, position);
				stopAnimation();
			};

			// Stop the animation
			if ( increments >= 0 ) { // If scrolling down
				// Stop animation when you reach the anchor OR the bottom of the page
				stopAnimation = function () {
					var travelled = window.pageYOffset;
					if ( (travelled >= (endLocation(anchor) - increments)) || ((window.innerHeight + travelled) >= document.body.scrollHeight) ) {
						clearInterval(runAnimation);
						updateURL(url, anchor);
					}
				};
			} else { // If scrolling up
				// Stop animation when you reach the anchor OR the top of the page
				stopAnimation = function () {
					var travelled = window.pageYOffset;
					if ( travelled <= endLocation(anchor) || travelled <= 0 ) {
						clearInterval(runAnimation);
						updateURL(url, anchor);
					}
				};
			}
			// Loop the animation function
			var runAnimation = setInterval(animateScroll, 16);
		};